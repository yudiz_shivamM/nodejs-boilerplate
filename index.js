const num_processes = require('os').cpus().length
const farmhash = require('farmhash')
const cluster = require('cluster')
const net = require('net')

const config = require('./config')
if (cluster.isMaster) {
    let workers = []
    for (var i = 0; i < num_processes; i++) spawn(i)
    net.createServer({
        pauseOnConnect: true
    }, (connection) => {
        let worker_index = farmhash.fingerprint32(connection.remoteAddress) % num_processes
        let worker = workers[worker_index]
        worker.send('sticky-session:connection', connection)
    }).listen(config.PORT, () => console.log(`Listining on ${config.PORT}`))

    function spawn(i) {
        workers[i] = cluster.fork()
        workers[i].on('exit', (code, signal) => spawn(i))
    }
} else {
    require('./app')
}