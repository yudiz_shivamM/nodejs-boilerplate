const _ = {}

_.parse = function (data) {
    return JSON.parse(data)
}

_.stringify = function (data, offset = 0) {
    return JSON.stringify(data, null, offset)
}

_.clone = function (data) {
    return this.parse(this.stringify(data))
}

_.pick = function (obj, array) {
    return array.reduce((acc, elem) => {
        if (this.clone(obj).hasOwnProperty(elem)) acc[elem] = obj[elem]
        return acc
    }, {})
}

_.ommit = function (obj, array) {
    let clonedObject = this.clone(obj)
    let objectKeys = Object.keys(clonedObject)
    return objectKeys.reduce((acc, elem) => {
        if (!array.includes(elem)) acc[elem] = clonedObject[elem];
        return acc
    }, {})
}

module.exports = _