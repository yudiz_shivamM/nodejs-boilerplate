const messages = {
    server_error: (prefix) => messages.prepare(500, prefix, 'server error'),
    not_found: (prefix) => messages.prepare(404, prefix, 'not found'),
    success: (prefix) => messages.prepare(200, prefix, 'success'),
    required_field: (prefix) => messages.prepare(419, prefix, 'Field required')
}

Object.defineProperty(messages, 'prepare', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: (code, prefix, message) => {
        return {
            code,
            message: `${prefix ? prefix + ' ' + message : message}`
        }
    }
});

module.exports = messages