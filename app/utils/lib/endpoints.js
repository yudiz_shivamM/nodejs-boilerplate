const endpoints = {}

endpoints.send = (res, { code, message }, data = {}) => {
    res.status(code).json({ message, data })
}

module.exports = endpoints