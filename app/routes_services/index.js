const bodyParser = require('body-parser')
const helmet = require('helmet')
const cors = require('cors')
const expressDevice = require('express-device')

module.exports = (app) => {
    /**
     *  Dependency Initialization
     */
    app.use(cors())
    app.use(helmet())
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json())
    app.use(expressDevice.capture())
    app.use((req, res, next) => {
        res.header('Access-Control-Expose-Headers', 'Authorization')
        next()
    });
    app.use(require('express').static('./public'))
    /**
     *  Routes definations
     */
    app.use('/app/v1/auth/', require('./auth'))
}