const AuthRouter = require('express').Router()

const validator = require('./lib/validators')
const middlewares = require('./lib/middlewares')
const controllers = require('./lib/controllers')

AuthRouter.post('/register', validator.registration, middlewares.registration, controllers.registration)

module.exports = AuthRouter