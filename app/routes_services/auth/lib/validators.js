const { _, endpoints, messages } = require('../../../utils')
const validators = {}

validators.registration = (req, res, next) => {
    const body = _.pick(req.body, ['username', 'password'])
    if(!body.username) return endpoints.send(res, messages.required_field('username'), {})
    return next()
}

module.exports = validators