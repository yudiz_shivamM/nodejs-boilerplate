const services = require('./services')
const { messages, endpoints, _ , log} = require('../../../utils')
const controllers = {}

controllers.registration = (req, res) => {
    const body = _.pick(req.body, ['username','password'])
    services.getUser(body, (error, response) => {
        if (error) return endpoints.sendResponse(res, messages.server_error(), error)
        log.red('Start', body, 'End')
        endpoints.send(res, messages.success(), response)
    })
}

module.exports = controllers