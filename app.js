const app = new require('express')()
const server = app.listen()

require('./app/routes_services')(app)

process.on('message', function (message, connection) {
    //console.log("message ::=> ", message)
    if (message !== 'sticky-session:connection') return
    server.emit('connection', connection)
    connection.resume()
})